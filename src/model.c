#include "model.h"
#include "layer.h"
#include "data.h"
#include "metrics.h"

#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>


model_t *model_create(unsigned depth, unsigned insize) {
	model_t *m = malloc(sizeof(model_t));
	m->depth = depth;
	m->insize = insize;
	m->layers = calloc(depth, sizeof(layer_t*));
	return m;
}

model_t *model_copy_arch(model_t* model) {
	model_t *copy = model_create(model->depth, model->insize);

	for (unsigned i = 0; i != model->depth; ++i)
		copy->layers[i] = layer_copy_arch(model->layers[i]);

	return copy;
}

void model_del(model_t *model) {
	for (unsigned i = 0; i != model->depth; ++i)
		layer_del(model->layers[i]);
	free(model);
}

model_t *model_add_layer(model_t *model, layertype_t type, ...) {
	// find first NULL layer
	layer_t **layer_ptr;
	unsigned layer_idx;
	for (layer_ptr = model->layers, layer_idx = 0;
			*layer_ptr != NULL && layer_idx != model->depth;
			++layer_ptr, ++layer_idx);
	if (layer_idx == model->depth)  // the net is already filled
		return NULL;

	// create layer
	layer_t *layer = malloc(sizeof(layer_t));
	layer->type = type;

	va_list args;
	va_start(args, type);
	unsigned insize = (layer_idx == 0) ? model->insize : layer_outsize(model->layers[layer_idx - 1]);
	switch(layer->type) {
		case activation: ; // as label cant be part of assignment
			activationtype_t activationtype = va_arg(args, activationtype_t);
			layer->params.activation = activation_create(insize, activationtype);
			break;
		case dense: ;
			unsigned outsize = va_arg(args, unsigned);
			layer->params.dense = dense_random(insize, outsize);
			break;
	}
	va_end(args);

	// set it to new layer
	*layer_ptr = layer;
	return model;
}

vec_t *model_apply(model_t *model, vec_t *input) {
	for (unsigned i = 0; i != model->depth; ++i)
		input = layer_apply(model->layers[i], input);
	return input;
}

vec_t *model_train_once(model_t *model, model_t *dmodel, vec_t *input, vec_t *output) {
	// compute neuron values
	input = vec_copy(input);  // we dont want to change it as its part of the dataset
	vec_t *neuron_values[model->depth + 1];
	for (unsigned i = 0; i != model->depth; ++i) {
		neuron_values[i] = vec_copy(input);
		input = layer_apply(model->layers[i], input);
	}
	neuron_values[model->depth] = input;  // final model output, no need to copy this one

	vec_t *doutput = vec_sub_from(vec_copy(output), neuron_values[model->depth]);

	for (unsigned i = model->depth; i != 0; --i)  // i is unsigned so i >= 0 will _always_ be true
		doutput = layer_train(model->layers[i - 1], dmodel->layers[i - 1], neuron_values[i - 1], doutput);
	vec_del(doutput);

	// free computed neuron values (except for the last layer)
	for (unsigned i = 0; i != model->depth; ++i)
		vec_del(neuron_values[i]);

	return neuron_values[model->depth];
}

void model_train_batch(model_t *model, double learnrate, unsigned batchsize, vec_t **inputs, vec_t **outputs, double *loss, double *acc) {
	model_t *dmodel = model_copy_arch(model);

	*loss = 0.0;
	*acc = 0.0;
	vec_t *model_output;
	for (unsigned i = 0; i != batchsize; ++i) {
		model_output = model_train_once(model, dmodel, inputs[i], outputs[i]);

		// iterative average
		*loss += (mean_squared_error(outputs[i], model_output) - *loss) / (i + 1);
		*acc += (categorical_accuracy(outputs[i], model_output) - *acc) / (i + 1);

		vec_del(model_output);
	}

	for (unsigned i = 0; i != model->depth; ++i)
		layer_update(model->layers[i], dmodel->layers[i], learnrate / batchsize);

	model_del(dmodel);
}

void model_train_epoch(model_t *model, double learnrate, unsigned batchsize, unsigned dataset_size, vec_t **inputs, vec_t **outputs) {
	unsigned num_full_batches = (dataset_size + 1) / batchsize - 1;  // if batchsize divides dataset_size, this will be dataset_size/batchsize - 1
	double loss = 0.0, acc = 0.0;

	for (unsigned i = 0; i != num_full_batches; ++i) {
		model_train_batch(model, learnrate, batchsize, inputs + i * batchsize, outputs + i * batchsize, &loss, &acc);
		printf("\r%3.0f%% done - batch %d/%d - loss=%.4f acc=%.4f", 100.0 * (i * batchsize) / dataset_size, i + 1, num_full_batches + 1, loss, acc);
		fflush(stdout);
	}

	// last (maybe incomplete) batch
	unsigned incomplete_batch = num_full_batches * batchsize;
	model_train_batch(model, learnrate, dataset_size - incomplete_batch, inputs + incomplete_batch, outputs + incomplete_batch, &loss, &acc);
	printf("\r%3.0f%% done - batch %d/%d - loss=%.4f acc=%.4f\n", 100.0, num_full_batches + 1, num_full_batches + 1, loss, acc);
}

void model_train_full(model_t *model, double learnrate, unsigned epochs, unsigned batchsize, unsigned dataset_size, vec_t **inputs, vec_t **outputs) {
	for (unsigned i = 0; i != epochs; ++i) {
		printf("epoch %d/%d:\n", i + 1, epochs);
		data_shuffle(dataset_size, inputs, outputs);
		model_train_epoch(model, learnrate, batchsize, dataset_size, inputs, outputs);
	}
}

void model_test(model_t *model, unsigned dataset_size, vec_t **inputs, vec_t **outputs, double *loss, double *acc) {
	*loss = 0.0;
	*acc = 0.0;
	vec_t *model_output;
	for (unsigned i = 0; i != dataset_size; ++i) {
		model_output = vec_copy(inputs[i]);
		model_output = model_apply(model, model_output);

		// iterative average
		*loss += (mean_squared_error(outputs[i], model_output) - *loss) / (i + 1);
		*acc += (categorical_accuracy(outputs[i], model_output) - *acc) / (i + 1);
	}
}
