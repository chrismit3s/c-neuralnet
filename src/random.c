#include "random.h"

#include <math.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>


#define SQRTPI (double)(1.77245385090551602729)


void random_seed() {
	static bool seeded = false;
	if (!seeded) {
		srand(time(NULL));
		seeded = true;
	}
}

// random double from unifrom [0,1)
double random_uniform() {
	return rand() / (RAND_MAX + 1.0);
}

// random double from gaussian normal distribution
double random_normal() {
	double sum = -6;
	for (int i = 0; i != 12; ++i)
		sum += random_uniform();
	return sum;
}

// random double from unifrom [a,b)
double random_double(double a, double b) {
	return (b - a) * random_uniform() + a;
}

// random int from unifrom [a,b)
int random_int(int a, int b) {
	return (int)((b - a) * random_uniform()) + a;
}
