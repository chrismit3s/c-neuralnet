#ifndef _MATRIX_H
#define _MATRIX_H


// TODO better error handling (fprintf(stderr..) + exit(1) on wrongly sized argument)


typedef struct {
	unsigned int n;  // size of the vector
	double *x;  // data
} vec_t;

typedef struct {
	unsigned int w;  // width of the matrix
	unsigned int h;  // height of the matrix
	double *x;  // data
} mat_t;


#define VEC_E(v, i) ((v)->x[(i)])
#define MAT_E(m, i, j) ((m)->x[(i) + (j) * (m)->w])


vec_t *vec_zeros(unsigned int n);
vec_t *vec_normal(unsigned int n);
vec_t *vec_from_args(unsigned int n, ...);
vec_t *vec_copy(vec_t *v);
void   vec_printf(vec_t *v, const char *fmt);
void   vec_del(vec_t *v);
vec_t *vec_set_all(vec_t *v, double s);
vec_t *vec_add_scalar(vec_t *v, double s);
vec_t *vec_mult_scalar(vec_t *v, double s);
vec_t *vec_add_to(vec_t *v, vec_t *w);
vec_t *vec_sub_from(vec_t *v, vec_t *w);
vec_t *vec_mult_by(vec_t *v, vec_t *w);

mat_t *mat_zeros(unsigned int w, unsigned int h);
mat_t *mat_uniform(unsigned w, unsigned h);
mat_t *mat_normal(unsigned int w, unsigned int h);
mat_t *mat_from_args(unsigned int w, unsigned int h, ...);
mat_t *mat_copy(mat_t *m);
void   mat_printf(mat_t *m, const char *fmt);
void   mat_del(mat_t *m);
mat_t *mat_add_scalar(mat_t *m, double s);
mat_t *mat_mult_scalar(mat_t *m, double s);
mat_t *mat_add_to(mat_t *m, mat_t *n);
mat_t *mat_sub_from(mat_t *m, mat_t *n);

vec_t *mat_vec_prod(mat_t *m, vec_t *v);
vec_t *vec_mat_prod(vec_t *v, mat_t *m);
mat_t *vec_outer_prod(vec_t *v, vec_t *w);


#endif /* _MATRIX_H */
