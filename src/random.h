#ifndef _RANDOM_H
#define _RANDOM_H


void random_seed();
double random_uniform();  // random double from unifrom [0,1)
double random_normal();  // random double from gaussian normal distribution
double random_double(double a, double b);  // random double from unifrom [a,b)
int random_int(int a, int b);  // random int from unifrom [a,b)


#endif /* _RANDOM_H */
