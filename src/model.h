#ifndef _MODEL_H
#define _MODEL_H

#include "layer.h"


// TODO better error handling everywhere in the project (no random crashes or just quietly returning NULL)


typedef struct {
	unsigned depth;
	unsigned insize;
	layer_t **layers;
} model_t;


model_t *model_create(unsigned depth, unsigned insize);
model_t *model_copy_arch(model_t* model);
void model_del(model_t *model);
model_t *model_add_layer(model_t *model, layertype_t type, ...);
vec_t *model_apply(model_t *model, vec_t *input);
vec_t *model_train_once(model_t *model, model_t *dmodel, vec_t *input, vec_t *output);
void model_train_batch(model_t *model, double learnrate, unsigned batchsize, vec_t **inputs, vec_t **outputs, double *loss, double *acc);
void model_train_epoch(model_t *model, double learnrate, unsigned batchsize, unsigned dataset_size, vec_t **inputs, vec_t **outputs);
void model_train_full(model_t *model, double learnrate, unsigned epochs, unsigned batchsize, unsigned dataset_size, vec_t **inputs, vec_t **outputs);
void model_test(model_t *model, unsigned dataset_size, vec_t **inputs, vec_t **outputs, double *loss, double *acc);

#endif /* _MODEL_H */
