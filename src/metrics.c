#include "metrics.h"
#include "helpers.h"

#include <math.h>


double mean_squared_error(vec_t *v, vec_t *w) {
	if (v->n != w->n)
		return NAN;

	double err;
	for (unsigned i = 0; i != v->n; ++i) {
		double diff = VEC_E(v, i) - VEC_E(w, i);
		err += diff * diff;
	}
	return err;
}

double categorical_accuracy(vec_t *v, vec_t *w) {
	if (v->n != w->n)
		return NAN;

	return (double)(argmax(v) == argmax(w));
}
