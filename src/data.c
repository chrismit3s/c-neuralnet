#include "data.h"
#include "helpers.h"
#include "random.h"

#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>


#define TRAIN_FILE "data/mnist_train.csv"
#define TRAIN_SIZE 60000
#define TEST_FILE "data/mnist_test.csv"
#define TEST_SIZE 10000

// the longest lines in the mnist csv is 4205 characters long, so this should be plenty enough, if not ¯\_(ツ)_/¯
#define BUFFER_SIZE 5000


unsigned data_load_mnist_train(vec_t ***xs, vec_t ***ys) {
	data_load_mnist(TRAIN_FILE, TRAIN_SIZE, xs, ys);
	return TRAIN_SIZE;
}

unsigned data_load_mnist_test(vec_t ***xs, vec_t ***ys) {
	data_load_mnist(TEST_FILE, TEST_SIZE, xs, ys);
	return TEST_SIZE;
}

void data_load_mnist(char *filename, unsigned n, vec_t ***xs, vec_t ***ys) {
	// allocate memory for all vectors
	*xs = malloc(sizeof(vec_t*) * n);
	*ys = malloc(sizeof(vec_t*) * n);

	char buf[BUFFER_SIZE] = "";  // this string _should_ be big enough to handle every line in mnist csv, if not ¯\_(ツ)_/¯
	unsigned i = 0;
	FILE *file = fopen(filename, "r");
	fgets(buf, BUFFER_SIZE, file); // skip the first line containing column titles
	while (fgets(buf, BUFFER_SIZE, file)) {
		parse_line_mnist(buf, *xs + i, *ys + i);
		++i;
	}
	fclose(file);
}

void parse_line_mnist(char *line, vec_t **x, vec_t **y) {
	// get the class in onehot encoding
	char c = line[0];  // the digit here determines the class
	*y = onehot(10, c - '0');  // ten classes 0 through 9

	// get the image data
	*x = vec_zeros(28 * 28);  // mnist images are 28x28
	int value = 0;
	unsigned i = 0;
	for (char *ptr = line + 2; *ptr != '\0'; ++ptr) {  // +2 as we skip the first character and the following comma
		if (*ptr == ',' || *ptr == '\n') {
			VEC_E(*x, i++) = value / 255.0;  // the values in mnist csv are 0 - 255, we want 0 - 1
			value = 0;
		}
		else {
			value *= 10;
			value += *ptr - '0';
		}
	}
}

void data_shuffle(unsigned n, vec_t **xs, vec_t **ys) {
	unsigned unshuffled_len = n, a;
	while (unshuffled_len > 0) {
		a = random_int(0, unshuffled_len);
		swap((void**)xs, a, unshuffled_len - 1);
		swap((void**)ys, a, unshuffled_len - 1);
		--unshuffled_len;
	}
}

void data_del(unsigned n, vec_t **xs, vec_t **ys) {
	for (unsigned i = 0; i != n; ++i) {
		vec_del(xs[i]);
		vec_del(ys[i]);
	}
	free(xs);
	free(ys);
}
