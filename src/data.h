#ifndef _DATA_H
#define _DATA_H

#include "matrix.h"


// TODO dataset struct for size, x and y

unsigned data_load_mnist_train(vec_t ***xs, vec_t ***ys);
unsigned data_load_mnist_test(vec_t ***xs, vec_t ***ys);
void data_load_mnist(char *filename, unsigned n, vec_t ***xs, vec_t ***ys);
void parse_line_mnist(char *line, vec_t **x, vec_t **y);
void data_shuffle(unsigned n, vec_t **xs, vec_t **ys);
void data_del(unsigned n, vec_t **xs, vec_t **ys);


#endif /* _DATA_H */
