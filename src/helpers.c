#include "helpers.h"

#include <stdlib.h>


double valmax(vec_t *v) {
	double max = VEC_E(v, 0);
	for (unsigned i = 1; i != v->n; ++i)
		if (VEC_E(v, i) > max)
			max = VEC_E(v, i);
	return max;
}

unsigned argmax(vec_t *v) {
	unsigned index = 0;
	for (unsigned i = 0; i != v->n; ++i)
		if (VEC_E(v, i) > VEC_E(v, index))
			index = i;
	return index;
}

vec_t *onehot(unsigned n, unsigned i) {
	if (i >= n)
		return NULL;
	vec_t *v = vec_zeros(n);
	VEC_E(v, i) = 1.0;
	return v;
}

void swap(void **data, unsigned a, unsigned b) {
	void *temp = data[a];
	data[a] = data[b];
	data[b] = temp;
}
