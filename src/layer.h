#ifndef _LAYER_H
#define _LAYER_H

#include "matrix.h"
#include "layers/dense.h"
#include "layers/activation.h"

#include <stdarg.h>


typedef enum {
	activation,
	dense,
} layertype_t;

typedef union {
	activation_t *activation;
	dense_t *dense;
} layers_t;

typedef struct {
	layertype_t type;
	layers_t params;
} layer_t;


layer_t *layer_create(layertype_t type, ...);
layer_t *layer_copy_arch(layer_t *layer);
void     layer_del(layer_t *layer);
vec_t *  layer_apply(layer_t *layer, vec_t *input);
vec_t *  layer_train(layer_t *layer, layer_t *dlayer, vec_t *input, vec_t *doutput);
void     layer_update(layer_t *layer, layer_t *dlayer, double learnrate);
unsigned layer_insize(layer_t *layer);
unsigned layer_outsize(layer_t *layer);


#endif /* _LAYER_H */
