#include "layer.h"

#include <stdlib.h>
#include <stdio.h>


layer_t *layer_create(layertype_t type, ...) {
	layer_t *l = malloc(sizeof(layer_t));
	l->type = type;

	va_list args;
	va_start(args, type);
	switch(l->type) {
		case activation: ; // as label cant be part of assignment
			unsigned size = va_arg(args, unsigned);
			activationtype_t activationtype = va_arg(args, activationtype_t);
			l->params.activation = activation_create(size, activationtype);
			break;
		case dense: ;
			unsigned insize = va_arg(args, unsigned);
			unsigned outsize = va_arg(args, unsigned);
			l->params.dense = dense_random(insize, outsize);
			break;
	}
	va_end(args);

	return l;
}

layer_t *layer_copy_arch(layer_t *layer) {
	layer_t *l = malloc(sizeof(layer_t));
	l->type = layer->type;

	switch(layer->type) {
		case activation: ; // as label cant be part of assignment
			unsigned size = layer->params.activation->size;
			activationtype_t activationtype = layer->params.activation->type;
			l->params.activation = activation_create(size, activationtype);
			break;
		case dense: ;
			unsigned insize = layer->params.dense->weights->w;
			unsigned outsize = layer->params.dense->weights->h;
			l->params.dense = dense_zeros(insize, outsize);
			break;
	}

	return l;
}


#define DEL_CASE(t, l) \
	case t: \
		t ## _del(l); \
		break

void layer_del(layer_t *layer) {
	if (layer == NULL)
		return;

	switch(layer->type) {
		DEL_CASE(activation, layer->params.activation);
		DEL_CASE(dense, layer->params.dense);
	}
	free(layer);
}


#define APPLY_CASE(t, l, i) \
	case t: \
		i = t ## _apply(l, i); \
		break

vec_t *layer_apply(layer_t *layer, vec_t *input) {
	switch(layer->type) {
		APPLY_CASE(activation, layer->params.activation, input);
		APPLY_CASE(dense, layer->params.dense, input);
	}
	return input;
}


#define TRAIN_CASE(t, l, p, i, o) \
	case t: \
		i = t ## _train(l, p, i, o); \
		break

vec_t *layer_train(layer_t *layer, layer_t *dlayer, vec_t *input, vec_t *doutput) {
	switch(layer->type) {
		TRAIN_CASE(activation, layer->params.activation, dlayer->params.activation, input, doutput);
		TRAIN_CASE(dense, layer->params.dense, dlayer->params.dense, input, doutput);
	}
	return input;  // is now dinput
}


#define UPDATE_CASE(t, l, p, y) \
	case t: \
		t ## _update(l, p, y); \
		break

void layer_update(layer_t *layer, layer_t *dlayer, double learnrate) {
	switch(layer->type) {
		UPDATE_CASE(activation, layer->params.activation, dlayer->params.activation, learnrate);
		UPDATE_CASE(dense, layer->params.dense, dlayer->params.dense, learnrate);
	}
}


unsigned layer_insize(layer_t *layer) {
	switch(layer->type) {
		case activation:
			return layer->params.activation->size;
		case dense:
			return layer->params.dense->weights->w;
	}
	return 0;
}

unsigned layer_outsize(layer_t *layer) {
	switch(layer->type) {
		case activation:
			return layer->params.activation->size;
		case dense:
			return layer->params.dense->weights->h;
	}
	return 0;
}
