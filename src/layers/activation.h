#ifndef _ACTIVATION_H
#define _ACTIVATION_H


#include "../matrix.h"


typedef enum {
	relu,
	sigmoid,
	swish,
	softmax,
} activationtype_t;

typedef struct {
	unsigned size;
	activationtype_t type;
} activation_t;


activation_t *activation_create(unsigned insize, unsigned outsize);
void activation_del(activation_t *layer);
vec_t *activation_apply(activation_t *layer, vec_t *input);
vec_t *activation_train(activation_t *layer, activation_t *dlayer, vec_t *input, vec_t *doutput);
void activation_update(activation_t *layer, activation_t *dlayer, double learnrate);


#endif /* _ACTIVATION_H */
