#include "dense.h"

#include <stdlib.h>
#include <math.h>


dense_t *dense_zeros(unsigned insize, unsigned outsize) {
	dense_t *l = malloc(sizeof(dense_t));
	l->bias = vec_zeros(outsize);
	l->weights = mat_zeros(insize, outsize);
	return l;
}

dense_t *dense_random(unsigned insize, unsigned outsize) {
	dense_t *l = malloc(sizeof(dense_t));
	l->bias = vec_zeros(outsize);
	//l->weights = mat_mult_scalar(mat_normal(insize, outsize), 1.0 / sqrt(insize));  // xavier init

	// glorot uniform init
	mat_t *w = mat_uniform(insize, outsize);
	w = mat_add_scalar(w, -0.5);
	w = mat_mult_scalar(w, 2 * sqrt(6.0 / (insize + outsize)));
	l->weights = w;

	return l;
}

void dense_del(dense_t *layer) {
	vec_del(layer->bias);
	mat_del(layer->weights);
	free(layer);
}

vec_t *dense_apply(dense_t *layer, vec_t *input) {
	if (layer->weights->w != input->n)
		return NULL;
	vec_t *output = vec_add_to(mat_vec_prod(layer->weights, input), layer->bias);
	vec_del(input);
	return output;
}

vec_t *dense_train(dense_t *layer, dense_t *dlayer, vec_t *input, vec_t *doutput) {
	if (layer->weights->w != input->n
			|| layer->bias->n != doutput->n
			|| layer->bias->n != dlayer->bias->n
			|| layer->weights->w != dlayer->weights->w
			|| layer->weights->h != dlayer->weights->h)
		return NULL;

	// update weights
	mat_t *dweights = vec_outer_prod(input, doutput);
	mat_add_to(dlayer->weights, dweights);
	mat_del(dweights);

	// update bias
	vec_t *dbias = vec_copy(doutput);
	vec_add_to(dlayer->bias, dbias);
	vec_del(dbias);

	vec_t *dinput = vec_mat_prod(doutput, layer->weights);
	vec_del(doutput);
	return dinput;
}

void dense_update(dense_t *layer, dense_t *dlayer, double learnrate) {
	mat_t *dweights = mat_mult_scalar(mat_copy(dlayer->weights), learnrate);
	mat_add_to(layer->weights, dweights);
	mat_del(dweights);

	vec_t *dbias = vec_mult_scalar(vec_copy(dlayer->bias), learnrate);
	vec_add_to(layer->bias, dbias);
	vec_del(dbias);
}
