#include "activation.h"
#include "../helpers.h"

#include <stdlib.h>
#include <math.h>


vec_t *relu_function(vec_t *input);
vec_t *relu_derivative(vec_t *input);
vec_t *sigmoid_function(vec_t *input);
vec_t *sigmoid_derivative(vec_t *input);
vec_t *swish_function(vec_t *input);
vec_t *swish_derivative(vec_t *input);
vec_t *softmax_function(vec_t *input);
vec_t *softmax_derivative(vec_t *input);


activation_t *activation_create(unsigned size, activationtype_t type) {
	activation_t *l = malloc(sizeof(activation_t));
	l->size = size;
	l->type = type;
	return l;
}


void activation_del(activation_t *layer) {
	free(layer);
}


#define APPLY_CASE(t, i) \
	case t: \
		t ## _function(i); \
		break

vec_t *activation_apply(activation_t *layer, vec_t *input) {
	if (input == NULL || input->n != layer->size)
		return NULL;

	switch (layer->type) {
		APPLY_CASE(relu, input);
		APPLY_CASE(sigmoid, input);
		APPLY_CASE(swish, input);
		APPLY_CASE(softmax, input);
	}

	return input;
}


#define TRAIN_CASE(t, i) \
	case t: \
		t ## _derivative(i); \
		break

vec_t *activation_train(activation_t *layer, activation_t *dlayer, vec_t *input, vec_t *doutput) {
	if (input->n != layer->size ||
			doutput->n != layer->size ||
			dlayer->size != layer->size)
		return NULL;

	input = vec_copy(input);
	switch (layer->type) {
		TRAIN_CASE(relu, input);
		TRAIN_CASE(sigmoid, input);
		TRAIN_CASE(swish, input);
		TRAIN_CASE(softmax, input);
	}
	vec_mult_by(doutput, input);
	vec_del(input);

	return doutput;
}

void activation_update(activation_t *layer __attribute__((unused)), activation_t *dlayer __attribute__((unused)), double learnrate __attribute__((unused))) {
	// nop
}


vec_t *relu_function(vec_t *input) {
	for (unsigned i = 0; i != input->n; ++i) {
		double xi = VEC_E(input, i);
		VEC_E(input, i) = (int)(xi > 0.0) * xi;
	}
	return input;
}

vec_t *relu_derivative(vec_t *input) {
	for (unsigned i = 0; i != input->n; ++i) {
		double xi = VEC_E(input, i);
		VEC_E(input, i) = (double)(xi > 0.0);
	}
	return input;
}


vec_t *sigmoid_function(vec_t *input) {
	for (unsigned i = 0; i != input->n; ++i) {
		double xi = VEC_E(input, i);
		double exi = exp(-xi);
		VEC_E(input, i) = 1 / (1 + exi);
	}
	return input;
}

vec_t *sigmoid_derivative(vec_t *input) {
	for (unsigned i = 0; i != input->n; ++i) {
		double xi = VEC_E(input, i);
		double exi = exp(-xi);
		VEC_E(input, i) = exi / (1 + (2 + exi) * exi);
	}
	return input;
}


vec_t *swish_function(vec_t *input) {
	for (unsigned i = 0; i != input->n; ++i) {
		double xi = VEC_E(input, i);
		double exi = exp(-xi);
		VEC_E(input, i) = xi / (1 + exi);
	}
	return input;
}

vec_t *swish_derivative(vec_t *input) {
	for (unsigned i = 0; i != input->n; ++i) {
		double xi = VEC_E(input, i);
		double exi = exp(-xi);
		VEC_E(input, i) = (1 + (1 + xi) * exi) / (1 + (2 + exi) * exi);
	}
	return input;
}


vec_t *softmax_function(vec_t *input) {
	vec_add_scalar(input, -valmax(input));

	double exp_sum = 0.0;
	for (unsigned i = 0; i != input->n; ++i) {
		VEC_E(input, i) = exp(VEC_E(input, i));
		exp_sum += VEC_E(input, i);
	}
	for (unsigned i = 0; i != input->n; ++i)
		VEC_E(input, i) /= exp_sum;

	return input;
}

vec_t *softmax_derivative(vec_t *input) {
	input = softmax_function(input);
	for (unsigned i = 0; i != input->n; ++i) {
		double xi = VEC_E(input, i);
		VEC_E(input, i) = xi * (1 - xi);
	}
	return input;
}
