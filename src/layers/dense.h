#ifndef _DENSE_H
#define _DENSE_H


#include "../matrix.h"


typedef struct {
	vec_t *bias;
	mat_t *weights;
} dense_t;


dense_t *dense_zeros(unsigned insize, unsigned outsize);
dense_t *dense_random(unsigned insize, unsigned outsize);
void dense_del(dense_t *layer);
vec_t *dense_apply(dense_t *layer, vec_t *input);
vec_t *dense_train(dense_t *layer, dense_t *dlayer, vec_t *input, vec_t *doutput);
void dense_update(dense_t *layer, dense_t *dlayer, double learnrate);


#endif /* _DENSE_H */
