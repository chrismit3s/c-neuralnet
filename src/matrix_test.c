#include "matrix.h"
#include <stdlib.h>
#include <stdio.h>


#define PRINT_FMT ("% .2f")


int main(void) {
	printf("creating zero vector of size 5...\n");
	vec_t *v1 = vec_zeros(5);
	vec_printf(v1, PRINT_FMT);
	vec_del(v1);

	printf("\ncreating 1,2,3 vector...\n");
	vec_t *v2 = vec_from_args(3, 1.0, 2.0, 3.0);
	vec_printf(v2, PRINT_FMT);
	vec_del(v2);

	printf("\ncreating 2,3,5,7 vector and copying it...\n");
	vec_t *v3 = vec_from_args(4, 2.0, 3.0, 5.0, 7.0);
	vec_t *v3_copy = vec_copy(v3);
	printf(" ");
	vec_printf(v3, PRINT_FMT);
	printf("=");
	vec_printf(v3_copy, PRINT_FMT);
	vec_del(v3);
	vec_del(v3_copy);

	printf("\ncreating 2,3,5,7 and -1,-2,-3,-4 vector and adding them...\n");
	vec_t *v4_1 = vec_from_args(4, 2.0, 3.0, 5.0, 7.0);
	vec_t *v4_2 = vec_from_args(4, -1.0, -2.0, -3.0, -4.0);
	printf(" ");
	vec_printf(v4_1, PRINT_FMT);
	printf("+");
	vec_printf(v4_2, PRINT_FMT);
	printf("=");
	vec_printf(vec_add_to(v4_1, v4_2), PRINT_FMT);
	vec_del(v4_1);
	vec_del(v4_2);

	printf("\ncreating zero matrix of size 5x3...\n");
	mat_t *m1 = mat_zeros(5, 3);
	mat_printf(m1, PRINT_FMT);
	mat_del(m1);

	printf("\ncreating [1,2,3],[4,5,6],[7,8,9] matrix...\n");
	mat_t *m2 = mat_from_args(3, 3, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0);
	mat_printf(m2, PRINT_FMT);
	mat_del(m2);

	printf("\ncreating [2,3],[5,7] matrix and copying it...\n");
	mat_t *m3 = mat_from_args(2, 2, 2.0, 3.0, 5.0, 7.0);
	mat_t *m3_copy = mat_copy(m3);
	mat_printf(m3, PRINT_FMT);
	mat_printf(m3_copy, PRINT_FMT);
	mat_del(m3);
	mat_del(m3_copy);

	printf("\ncreating [2,3],[5,7] and [-1,-2],[-3,-4] matrix and adding them...\n");
	mat_t *m4_1 = mat_from_args(2, 2, 2.0, 3.0, 5.0, 7.0);
	mat_t *m4_2 = mat_from_args(2, 2, -1.0, -2.0, -3.0, -4.0);
	mat_printf(m4_1, PRINT_FMT);
	printf("+\n");
	mat_printf(m4_2, PRINT_FMT);
	printf("=\n");
	mat_printf(mat_add_to(m4_1, m4_2), PRINT_FMT);
	mat_del(m4_1);
	mat_del(m4_2);

	return EXIT_SUCCESS;
}
