#ifndef _HELPERS_H
#define _HELPERS_H

#include "matrix.h"


double valmax(vec_t *v);
unsigned argmax(vec_t *v);
vec_t *onehot(unsigned n, unsigned i);
void swap(void **data, unsigned a, unsigned b);


#endif /* _HELPERS_H */
