#ifndef _METRICS_H
#define _METRICS_H

#include "matrix.h"


double mean_squared_error(vec_t *v, vec_t *w);
double categorical_accuracy(vec_t *v, vec_t *w);


#endif /* _LOSS_H */
