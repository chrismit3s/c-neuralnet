#include "model.h"
#include "helpers.h"
#include "matrix.h"
#include "random.h"
#include "data.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>


#define INPUTWIDTH 28
#define INPUTSIZE (INPUTWIDTH * INPUTWIDTH)
#define OUTPUTSIZE 10

#define BATCHSIZE 400
#define EPOCHS 15
#define LEARNRATE 0.04


int main(void) {
	random_seed();

	// build model
	printf("building model...\n");
	model_t *model = model_create(10, INPUTSIZE);
	model_add_layer(model, dense, 64);
	model_add_layer(model, activation, relu);
	model_add_layer(model, dense, 64);
	model_add_layer(model, activation, relu);
	model_add_layer(model, dense, 32);
	model_add_layer(model, activation, relu);
	model_add_layer(model, dense, 32);
	model_add_layer(model, activation, relu);
	model_add_layer(model, dense, OUTPUTSIZE);
	model_add_layer(model, activation, softmax);

	// load training data
	printf("loading training data...\n");
	vec_t **x_train;
	vec_t **y_train;
	unsigned dataset_size = data_load_mnist_train(&x_train, &y_train);
	printf("shuffling training data...\n");
	data_shuffle(dataset_size, x_train, y_train);

	// train model
	printf("training model...\n");
	model_train_full(model, LEARNRATE, EPOCHS, BATCHSIZE, dataset_size, x_train, y_train);

	// load training data
	printf("loading testing data...\n");
	vec_t **x_test;
	vec_t **y_test;
	unsigned testing_size = data_load_mnist_test(&x_test, &y_test);
	printf("shuffling testing data...\n");
	data_shuffle(testing_size, x_test, y_test);

	// test model
	printf("training model...\n");
	double loss, acc;
	model_test(model, testing_size, x_test, y_test, &loss, &acc);
	printf("loss=%.4f acc=%.4f\n", loss, acc);

	vec_t *in = vec_zeros(INPUTSIZE);
	unsigned line = 0, col = 0;
	printf("draw a number using \'#+:. \' in 28 columns by 28 lines:\n");
	printf("   0123456789012345678901234567\n");
	printf("%2d ", line++);
	fflush(stdout);
	while (line * INPUTWIDTH + col < INPUTSIZE) {
		char c = fgetc(stdin);
		if (c == '\n') {
			printf("%2d ", line++);
			fflush(stdout);
			col = 0;
			continue;
		}
		if (col >= INPUTWIDTH) {
			continue;
		}

		float value = 0.0;
		switch (c) {
			case '#': value = 1.00; break;
			case '+': value = 0.75; break;
			case ':': value = 0.50; break;
			case '.': value = 0.25; break;
			case ' ': value = 0.00; break;
			default:
				printf("unknown char \'%c\'\n", c);
				continue;
		}
		VEC_E(in, line * INPUTWIDTH + col) = value;
		col++;
	}
	printf("\n");

	in = model_apply(model, in);
	unsigned m = argmax(in);
	printf("predition:\n");
	for (unsigned i = 0; i != in->n; i++) {
		printf("%d: %.3f %s\n", i, VEC_E(in, i), (i == m) ? "<---" : "");
	}

	// cleanup
	vec_del(in);
	data_del(dataset_size, x_train, y_train);
	data_del(testing_size, x_test, y_test);
	model_del(model);

	return EXIT_SUCCESS;
}
