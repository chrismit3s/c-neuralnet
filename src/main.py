from keras.models import Sequential
from keras.layers import Dense, Flatten
from keras.datasets import mnist
from keras.optimizers import SGD
from keras.utils import to_categorical


inputsize = (28, 28)
outputsize = 10

batchsize = 120
epochs = 60
learnrate = 0.05


def build(**kwargs):
    m = Sequential()

    m.add(Flatten(input_shape=inputsize))
    m.add(Dense(64, activation="relu"))
    m.add(Dense(64, activation="relu"))
    m.add(Dense(64, activation="relu"))
    m.add(Dense(32, activation="relu"))
    m.add(Dense(32, activation="relu"))
    m.add(Dense(32, activation="relu"))
    m.add(Dense(outputsize, activation="softmax"))

    m.compile(**kwargs)
    return m


def main():
    # data
    print("loading data...")
    (x_train, y_train), (x_test, y_test) = mnist.load_data()
    y_train = to_categorical(y_train)
    y_test = to_categorical(y_test)

    # model
    print("building model...")
    m = build(loss="mse",
              optimizer=SGD(learning_rate=learnrate),
              metrics=["acc", "mse"])
    m.summary()

    print("training...")
    m.fit(x_train, y_train,
          batch_size=batchsize,
          epochs=epochs,
          verbose=1,
          validation_data=(x_test, y_test))


if __name__ == "__main__":
    main()
