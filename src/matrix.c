#include "matrix.h"
#include "random.h"

#include <math.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


vec_t *vec_zeros(unsigned n) {
	vec_t *v = malloc(sizeof(vec_t));
	v->n = n;
	v->x = calloc(n, sizeof(double));
	return v;
}

vec_t *vec_normal(unsigned n) {
	vec_t *v = vec_zeros(n);
	for (unsigned i = 0; i != v->n; ++i)
		VEC_E(v, i) = random_normal();
	return v;
}

vec_t *vec_from_args(unsigned n, ...) {
	vec_t *v = vec_zeros(n);

	va_list args;
	va_start(args, n);
	for (unsigned i = 0; i != v->n; ++i)
		VEC_E(v, i) = va_arg(args, double);
	va_end(args);

	return v;
}

vec_t *vec_copy(vec_t *v) {
	vec_t *r = vec_zeros(v->n);
	memcpy(r->x, v->x, v->n * sizeof(double));
	return r;
}

void vec_printf(vec_t *v, const char *fmt) {
	printf("( ");
	for (unsigned i = 0; i != v->n; ++i) {
		printf(fmt, VEC_E(v, i));
		printf(" ");
	}
	printf(")\n");
}

void vec_del(vec_t *v) {
	free(v->x);
	free(v);
}

vec_t *vec_set_all(vec_t *v, double s) {
	for (unsigned i = 0; i != v->n; ++i)
		VEC_E(v, i) = s;

	return v;
}

vec_t *vec_add_scalar(vec_t *v, double s) {
	for (unsigned i = 0; i != v->n; ++i)
		VEC_E(v, i) += s;

	return v;
}

vec_t *vec_mult_scalar(vec_t *v, double s) {
	for (unsigned i = 0; i != v->n; ++i)
		VEC_E(v, i) *= s;

	return v;
}

vec_t *vec_add_to(vec_t *v, vec_t *w) {
	if (v->n != w->n)
		return NULL;

	for (unsigned i = 0; i != v->n; ++i)
		VEC_E(v, i) += VEC_E(w, i);

	return v;
}

vec_t *vec_sub_from(vec_t *v, vec_t *w) {
	if (v->n != w->n)
		return NULL;

	for (unsigned i = 0; i != v->n; ++i)
		VEC_E(v, i) -= VEC_E(w, i);

	return v;
}

vec_t *vec_mult_by(vec_t *v, vec_t *w) {
	if (v->n != w->n)
		return NULL;

	for (unsigned i = 0; i != v->n; ++i)
		VEC_E(v, i) *= VEC_E(w, i);

	return v;
}


mat_t *mat_zeros(unsigned w, unsigned h) {
	mat_t *m = malloc(sizeof(mat_t));
	m->w = w;
	m->h = h;
	m->x = calloc(w * h, sizeof(double));
	return m;
}

mat_t *mat_uniform(unsigned w, unsigned h) {
	mat_t *m = mat_zeros(w, h);
	for (unsigned j = 0; j != m->h; ++j)
		for (unsigned i = 0; i != m->w; ++i)
			MAT_E(m, i, j) = random_uniform();
	return m;
}

mat_t *mat_normal(unsigned w, unsigned h) {
	mat_t *m = mat_zeros(w, h);
	for (unsigned j = 0; j != m->h; ++j)
		for (unsigned i = 0; i != m->w; ++i)
			MAT_E(m, i, j) = random_normal();
	return m;
}

mat_t *mat_from_args(unsigned w, unsigned h, ...) {
	mat_t *m = mat_zeros(w, h);

	va_list args;
	va_start(args, h);
	for (unsigned j = 0; j != m->h; ++j)
		for (unsigned i = 0; i != m->w; ++i)
			MAT_E(m, i, j) = va_arg(args, double);
	va_end(args);

	return m;
}

mat_t *mat_copy(mat_t *m) {
	mat_t *r = mat_zeros(m->w, m->h);
	memcpy(r->x, m->x, m->w * m->h * sizeof(double));
	return r;
}

void mat_printf(mat_t *m, const char *fmt) {
	for (unsigned j = 0; j != m->h; ++j) {
		if (j == 0)
			printf("/ ");
		else if (j == (m->h - 1))
			printf("\\ ");
		else
			printf("| ");

		for (unsigned i = 0; i != m->w; ++i) {
			printf(fmt, MAT_E(m, i, j));
			printf(" ");
		}

		if (j == 0)
			printf("\\\n");
		else if (j == (m->h - 1))
			printf("/\n");
		else
			printf("|\n");
	}
}

void mat_del(mat_t *m) {
	free(m->x);
	free(m);
}

mat_t *mat_add_scalar(mat_t *m, double s) {
	for (unsigned i = 0; i != m->w; ++i)
		for (unsigned j = 0; j != m->h; ++j)
			MAT_E(m, i, j) += s;

	return m;
}

mat_t *mat_mult_scalar(mat_t *m, double s) {
	for (unsigned i = 0; i != m->w; ++i)
		for (unsigned j = 0; j != m->h; ++j)
			MAT_E(m, i, j) *= s;

	return m;
}

mat_t *mat_add_to(mat_t *m, mat_t *n) {
	if (m->w != n->w || m->h != n->h)
		return NULL;

	for (unsigned i = 0; i != m->w; ++i)
		for (unsigned j = 0; j != m->h; ++j)
			MAT_E(m, i, j) += MAT_E(n, i, j);

	return m;
}

mat_t *mat_sub_from(mat_t *m, mat_t *n) {
	if (m->w != n->w || m->h != n->h)
		return NULL;

	for (unsigned i = 0; i != m->w; ++i)
		for (unsigned j = 0; j != m->h; ++j)
			MAT_E(m, i, j) -= MAT_E(n, i, j);

	return m;
}


vec_t *mat_vec_prod(mat_t *m, vec_t *v) {
	if (m->w != v->n)
		return NULL;

	vec_t *r = vec_zeros(m->h);
	for (unsigned j = 0; j != m->h; ++j)
		for (unsigned i = 0; i != m->w; ++i)
			VEC_E(r, j) += VEC_E(v, i) * MAT_E(m, i, j);
	return r;
}

vec_t *vec_mat_prod(vec_t *v, mat_t *m) {
	if (m->h != v->n)
		return NULL;

	vec_t *r = vec_zeros(m->w);
	for (unsigned i = 0; i != m->w; ++i)
		for (unsigned j = 0; j != m->h; ++j)
			VEC_E(r, i) += VEC_E(v, j) * MAT_E(m, i, j);
	return r;
}

mat_t *vec_outer_prod(vec_t *v, vec_t *w) {
	mat_t *r = mat_zeros(v->n, w->n);
	for (unsigned i = 0; i != r->w; ++i)
		for (unsigned j = 0; j != r->h; ++j)
			MAT_E(r, i, j) = VEC_E(v, i) * VEC_E(w, j);
	return r;
}
