#include "model.h"
#include "matrix.h"
#include "random.h"

#include <stdio.h>
#include <stdlib.h>


#define DATASET_SIZE 60000
#define BATCHSIZE 60
#define EPOCHS 30
#define LEARNRATE 0.01


int main(void) {
	random_seed();

	// build model
	model_t *model = model_create(3, 2);
	model_add_layer(model, dense, 64);
	model_add_layer(model, activation, relu);
	model_add_layer(model, dense, 1);

	// create data
	vec_t *x[DATASET_SIZE] = { 0 };
	vec_t *y[DATASET_SIZE] = { 0 };
	for (unsigned i = 0; i != DATASET_SIZE; ++i) {
		double a = random_normal();
		double b = random_normal();
		x[i] = vec_from_args(2, a, b);
		y[i] = vec_from_args(1, (a > b) ? a : b);
	}

	model_train_full(model, LEARNRATE, EPOCHS, BATCHSIZE, DATASET_SIZE, x, y);

	vec_t *out;
	out = model_apply(model, vec_from_args(2, -1.0, 1.0));
	printf("1 => %f\n", VEC_E(out, 0));
	vec_del(out);

	out = model_apply(model, vec_from_args(2, 2.0, 7.0));
	printf("7 => %f\n", VEC_E(out, 0));
	vec_del(out);

	out = model_apply(model, vec_from_args(2, 0.6, 0.09));
	printf("0.6 => %f\n", VEC_E(out, 0));
	vec_del(out);

	// cleanup
	for (unsigned i = 0; i != DATASET_SIZE; ++i) {
		vec_del(x[i]);
		vec_del(y[i]);
	}
	model_del(model);

	return EXIT_SUCCESS;
}
