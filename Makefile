.PHONY: all test


cc := gcc -O3 -Wall -Wextra -Werror -pedantic
rm := del


all: bin/main.exe

test: bin/matrix_test.exe bin/model_test.exe

bin/obj/random.o: src/random.c src/random.h
	${cc} -c -o bin/obj/random.o src/random.c

bin/obj/helpers.o: src/helpers.c src/helpers.h
	${cc} -c -o bin/obj/helpers.o src/helpers.c

bin/obj/matrix.o: src/matrix.c src/matrix.h
	${cc} -c -o bin/obj/matrix.o src/matrix.c

bin/obj/layer.o: src/layers/dense.c src/layers/dense.h src/layers/activation.c src/layers/activation.h src/layer.h src/layer.c src/matrix.h src/helpers.h
	${cc} -c -o bin/obj/dense.o src/layers/dense.c
	${cc} -c -o bin/obj/activation.o src/layers/activation.c -lm
	${cc} -c -o bin/obj/layer.o src/layer.c

bin/obj/metrics.o: src/metrics.c src/metrics.h
	${cc} -c -o bin/obj/metrics.o src/metrics.c

bin/obj/model.o: src/model.c src/model.h
	${cc} -c -o bin/obj/model.o src/model.c

bin/obj/data.o: src/data.c src/data.h
	${cc} -c -o bin/obj/data.o src/data.c

bin/neural.lib: bin/obj/*
	ar rcs bin/neural.lib bin/obj/*

bin/matrix_test.exe: src/matrix_test.c bin/obj/matrix.o bin/obj/random.o
	${cc} -o bin/matrix_test.exe src/matrix_test.c bin/obj/matrix.o bin/obj/random.o -lm

bin/model_test.exe: src/model_test.c bin/obj/model.o bin/obj/layer.o bin/obj/data.o bin/obj/random.o bin/obj/metrics.o
	${cc} -o bin/model_test.exe src/model_test.c bin/obj/* -lm

bin/main.exe: bin/neural.lib src/main.c
	${cc} -o bin/main.exe src/main.c bin/neural.lib -lm
